# Build "builder" image
FROM microsoft/dotnet:sdk AS build-env
WORKDIR /app

COPY ./MyEpicApp/*.csproj .

RUN dotnet restore

COPY ./MyEpicApp ./
RUN dotnet publish -c Release -o out

# Build runtime image
FROM microsoft/dotnet:aspnetcore-runtime
WORKDIR /app
COPY --from=build-env /app/out .
ENTRYPOINT ["dotnet", "MyEpicApp.dll"]