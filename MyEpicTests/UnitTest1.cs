using System;
using MyEpicApp;
using NUnit.Framework;

namespace Tests
{
    public class Tests
    {
        private Program program;

        [SetUp]
        public void Setup()
        {
            this.program = new Program();
            Console.WriteLine("Setup Run!");
        }

        [TestCase(2, 2, ExpectedResult = 4)]
        [TestCase(1, 1, ExpectedResult = 2)]
        [TestCase(2, 3, ExpectedResult = 5)]
        public int MyProgramAddTest(int a, int b)
        {
            return program.Add( a, b );
        }

        public void testDown()
        {
            this.program = null;
        }
    }
}